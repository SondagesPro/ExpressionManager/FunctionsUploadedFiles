<?php

/**
 * This file is part of FunctionsUploadedFiles plugin
 * @version 0.1.0
 */

namespace FunctionsUploadedFiles;

use App;
use LimeExpressionManager;
use Survey;
use Question;
use CHtml;

class UploadedFiles
{
    /** @var array[][] uploadQuestionColumns : do only one SQL request if use function multiple times **/
    private static $uploadQuestionColumns;

    /**
     * Return function as static
     * @param string $qCode : the upload question code
     * @param string $type : the upload question code in array : [title,comment,size,name,filename,ext,url,link]
     * @param integer $index : the index of the file
     * @return string empty string is file didn't exist or are invalid
     */
    public static function fileInfo($qCode, $type = 'link', $index = 0)
    {
        $test = "";
        /* get the current response Id */
        $surveyId = LimeExpressionManager::getLEMsurveyId();
        if (!Survey::model()->findByPk($surveyId)->getIsActive()) {
            return "";
        }
        $sessionSurvey = App()->session['survey_' . $surveyId];
        if (empty($sessionSurvey['srid'])) {
            return "";
        }
        /* Find question */
        $questionUpload = self::getUploadQuestionColumn($surveyId, $qCode);
        if ($questionUpload === "") {
            return "";
        }
        if (empty($sessionSurvey[$questionUpload])) {
            return "";
        }
        /* We have something */
        $fileList = @json_decode($sessionSurvey[$questionUpload], 1);
        if (empty($fileList[$index])) {
            return "";
        }
        /* title,comment,size,name,filename,ext */
        if ($type && isset($fileList[$index][$type])) {
            return $fileList[$index][$type];
        }
        $url = App()->createUrl(
            'uploader/index',
            [
                'actionID' => $surveyId,
                'filegetcontents' => $fileList[$index]['filename'],
            ]
        );
        if ($type == 'url') {
            return $url;
        }
        return CHtml::link(
            $fileList[$index]['name'],
            $url,
            ['download' => $fileList[$index]['name'], 'class' => 'ls-uploadedfile-link' ]
        );
    }

    /**
     * Return the question SGQA of and upload question type
     * @param $surveyId the survey id
     * @param $qCode the question code
     * @return string
     */
    private static function getUploadQuestionColumn($surveyId, $qCode)
    {
        if (isset(self::$uploadQuestionColumns[$surveyId][$qCode])) {
            return self::$uploadQuestionColumns[$surveyId][$qCode];
        }
        $question = Question::model()->find([
            'select' => ['sid', 'gid', 'qid', 'title'],
            'condition' => "sid = :sid AND title = :title and type = :type",
            'params' => [':sid' => $surveyId, ':title' => $qCode, ':type' => "|"]
        ]);
        if (empty($question)) {
            self::$uploadQuestionColumns[$surveyId][$qCode] = "";
            return "";
        }
        self::$uploadQuestionColumns[$surveyId][$qCode] = $surveyId . 'X' . $question->gid . 'X' . $question->qid;
        return self::$uploadQuestionColumns[$surveyId][$qCode];
    }
}
