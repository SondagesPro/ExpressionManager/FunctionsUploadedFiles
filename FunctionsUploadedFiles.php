<?php

/**
 * @copyright 2024 Debnis Chenu / Sondages Pro <https://sondages.pro>
 * @author Denis Chenu <denis@sondages.pro>
 * @license AGPL version 3
 * @version 0.1.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class FunctionsUploadedFiles extends PluginBase
{
    protected static $description = 'Function to get information from uploaded files in current survey : {fileInfo(qCode [, infoType = \'link\' [, index = 0]])}.]';
    protected static $name = 'FunctionsUploadedFiles';


    /** @inheritdoc, this plugin settings are update during getSettings */
    protected $settings = array(
        'information' => array(
            'type' => 'info',
            'content' => '',
            'default' => false
        ),
    );

    /** @inheritdoc, this plugin didn't have any public method */
    public $allowedPublicMethods = array();

    public function init()
    {
        $this->subscribe('ExpressionManagerStart', 'newValidFunctions');
    }

    /**
     * @see https://manual.limesurvey.org/ExpressionManagerStart ExpressionManagerStart event
     * add the getAnswerOptionText static function to Expression Manager function
     * @return void
     */
    public function newValidFunctions()
    {
        Yii::setPathOfAlias("FunctionsUploadedFiles", dirname(__FILE__));
        $newFunctions = array(
            'UploadedFiles_fileInfo' => array(
                '\FunctionsUploadedFiles\UploadedFiles::fileInfo',
                null, // No javascript function : set as static function
                $this->gT("Return the information from uploaded files in current survey."), // Description for admin
                'string UploadedFiles_fileInfo(qcode[, type[, index]])', // usage
                'https://support.sondages.pro', // Help url
                1, 2, 3 // 1 or 2 or 3 argument
            ),
        );
        $this->getEvent()->append('functions', $newFunctions);
    }

    /**
     * @inheritdoc
     * Update the information content
     */
    public function getPluginSettings($getValues = true)
    {
        $this->subscribe('getPluginTwigPath');
        $lang = array(
            "Function to get information from uploaded files in current survey." => $this->gT("Function to get information from uploaded files in current survey."),
            "Usage : %s" => $this->gT("Usage : %s"),
            "%s : the upload question code" => $this->gT("%s : the upload question code"),
            "%s : the file information type in array : %s" => $this->gT("%s : the upload question code in array : %s"),
            "%s : the index of the file" => $this->gT("%s : the index of the file"),
        );
        $content = Yii::app()->twigRenderer->renderPartial('/FunctionsUploadedFilesInfo.twig', array(
            'lang' => $lang
        ));
        $this->settings['information']['content'] = $content;
        return parent::getPluginSettings($getValues);
    }

    /**
     * Add some views for this and other plugin
     */
    public function getPluginTwigPath()
    {
        $viewPath = dirname(__FILE__) . "/views";
        $this->getEvent()->append('add', array($viewPath));
    }

    /**
     * @inheritdoc
     * But do nothing : avoid call of DummyStorage
     */
    public function saveSettings($settings)
    {
        // Nothing saved, not needed
    }
}
