# FunctionsUploadedFiles

A LimeSurvey plugin.

Expression manager function to get information from uploaded files in current survey.

## Usage : 

`{UploadedFiles_fileInfo($qCode [, $type = 'link' [, $index = 0]])}`

- @param string `qCode` : the upload question code
- @param string `type` : the upload question code in array : `['title','comment','size','name','ext','filename','url','link']`
    - `title` return the title set by participant
    - `comment` return the comment set by participant
    - `size` return the file size estimated in byte
    - `name` return the name of the file uploaded by participant.
    - `ext` return the extension of the file uploaded by participant.
    - `filename` return the filename of the uploaded file (in upload directory)
    - `url` return the url to the get file content via uploader controller
    - `link` return a complete link to the file with `download` attribute with name and ls-uploadedfile-link for class.
- @param integer `index` : the index of the file


## Home page & Copyright
- HomePage <http://extensions.sondages.pro/>
- Copyright © 2024 Denis Chenu <https://sondages.pro>
- Licence : GNU Affero General Public License <https://www.gnu.org/licenses/agpl-3.0.html>
- [Donate](https://support.sondages.pro/open.php?topicId=12), [Liberapay](https://liberapay.com/SondagesPro/), [OpenCollective](https://opencollective.com/sondagespro) 
